﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using OdomYangProject1.DAL.Interfaces;
using OdomYangProject1.DAL.Repository;
using OdomYangProject1.Model;

namespace OdomYangProject1.Controller
{
    public class EmployeeController
    {
        private readonly IEmployee employeeRepo;
        public EmployeeController()
        {
            this.employeeRepo = new EmployeeRepository();
        }

        /// <summary>
        /// Checks to see if employee exists in the database
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns> true or false </returns>
        public Boolean DoesEmployeeExist(string username, string password)
        {
            Boolean doesExist = false;
            try
            {
               
                Employee employee = this.employeeRepo.FindEmployeeByLogin(username, password);
                if (employee.EmployeeID != 0)
                {
                    doesExist = true;
                }
            }
            catch (Exception)
            {

                throw;
            }

            return doesExist;
        }

        /// <summary>
        /// Gets the currently logged in employee.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        public Employee GetLoggedInEmployee(string username, string password)
        {
            Employee employee;
            try
            {
                
                employee = this.employeeRepo.FindEmployeeByLogin(username, password);
            }
            catch (Exception)
            {
                throw new Exception("Error");
            }
            return employee;
        }

    }
}
