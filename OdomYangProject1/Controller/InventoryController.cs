﻿using System;
using System.Collections.Generic;
using OdomYangProject1.DAL.Interfaces;
using OdomYangProject1.DAL.Repository;
using OdomYangProject1.Model;

namespace OdomYangProject1.Controller
{
    public class InventoryController
    {
        private readonly IInventory inventoryRepo;

        public InventoryController()
        {
            this.inventoryRepo = new InventoryRepository();
        }

        /// <summary>
        /// Sorts the inventory by criteria.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <param name="value">The value.</param>
        /// <returns> a collection of inventory filtered by criteria</returns>
        public List<Inventory> SortInventoryByCriteria(string criteria, string value)
        {
            List<Inventory> sortedInventory = new List<Inventory>();
            
            switch (criteria)
            {
                case "Style":
                    sortedInventory = this.inventoryRepo.FindInventoryByStyle(value);
                    break;
                case "Category":
                    sortedInventory = this.inventoryRepo.FindInventoryByCategory(value);
                    break;
                case "Inventory #":
                    sortedInventory.Add(this.inventoryRepo.GetByID(Convert.ToInt32(value)));
                    break;
                case "All":
                    sortedInventory = (List<Inventory>)this.inventoryRepo.GetAll();
                    break;
            }

            return sortedInventory;
        }

        public void DecrementInventory(int id, int amount)
        {
            
            this.inventoryRepo.DecrementStock(id,amount);
        }

        public void IncrementInventory(int id, int amount)
        {
            this.inventoryRepo.IncrementStock(id, amount);
        }


    }
}
