﻿using System;
using System.Collections.Generic;
using OdomYangProject1.DAL.Interfaces;
using OdomYangProject1.DAL.Repository;
using OdomYangProject1.Model;

namespace OdomYangProject1.Controller
{
    public class TransactionController
    {
        private ITransaction transactionRepository;
        private IRental rentalRepository;
        private IReturn returnRepository;
      
        public TransactionController()
        {
            this.transactionRepository = new TransactionRepository();
            this.rentalRepository = new RentalRepository();
            this.returnRepository = new ReturnRepository();
        }



        public List<Transaction> GetTransactionsByCustomer(int customerID)
        {
            List<Transaction> customerRentals = this.transactionRepository.GetCustomerTransactions(customerID);
            return customerRentals;
        }

        public List<Inventory> GetInventoryByRentalID(int rentalID)
        {
            List<Inventory> inventory = new List<Inventory>();
            inventory = this.rentalRepository.GetInventoryByRentalID(rentalID);
            return inventory;
        }

        public Rental GetRentalByID(int rentalID, int inventoryID)
        {
            Rental rental = new Rental();
            rental = this.rentalRepository.GetByID(rentalID, inventoryID);
            return rental;
        }

        public List<Return> GetReturnsByRentalID(int id, int inventoryID)
        {
            List<Return> returns = new List<Return>();
            returns = this.returnRepository.GetReturnByRentalID(id, inventoryID);
            return returns;
        }

        public Rental GetRentalByID(int id)
        {
            Rental rental = new Rental();
            rental = this.rentalRepository.GetByID(id);
            return rental;
        }

        public List<Rental> GetAllRentals()
        {
            List<Rental> rentals = (List<Rental>)this.rentalRepository.GetAll();
            return rentals;
        }

        public List<Return> GetAllReturns()
        {
            List<Return> returns = (List<Return>)this.returnRepository.GetAll();
            return returns;
        }

        public void addReturn(Transaction transaction, List<Return> returns)
        {
            this.transactionRepository.Add(transaction);
            foreach (Return aReturn in returns)
            {
                aReturn.ReturnTransactionID = transaction.TransactionID;
                this.returnRepository.Add(aReturn);
            }
        }

        public void addRental(Transaction transaction, List<Rental> rentals )
        {  
            this.transactionRepository.Add(transaction);
            foreach(Rental aRental in rentals)
            {
                aRental.RentalTransactionID = transaction.TransactionID;
                this.rentalRepository.Add(aRental);
            }
            
        }

        public int findMaxTransId()
        {
            return this.transactionRepository.findMaxId();
        }

        
    }
}
