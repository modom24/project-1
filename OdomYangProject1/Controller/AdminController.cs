﻿using OdomYangProject1.DAL.Interfaces;
using OdomYangProject1.DAL.Repository;
using OdomYangProject1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdomYangProject1.Controller
{
    
   public class AdminController
    {
        private readonly IAdmin adminRepo;

        public AdminController()
        {
            this.adminRepo = new AdminRepository();
        }

        public List<dynamic> QueryDB(String query)
        {
            List<dynamic> queryResults = this.adminRepo.QueryDB(query);
            return queryResults;
        }

        public Boolean DoesAdminExist(String username, String password)
        {
            Boolean doesExist = false;
            try
            {
                Admin admin = this.adminRepo.FindAdminByLogin(username, password);
                if (admin.AdminID != 0)
                {
                    doesExist = true;
                }
            }
            catch (Exception)
            {

                throw;
            }

            return doesExist;
        }

       public List<dynamic> AdminQuery(string sql)
       {
           return this.adminRepo.QueryDB(sql);
       } 

    }
}
