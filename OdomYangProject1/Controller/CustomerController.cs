﻿using System;
using System.Collections.Generic;
using OdomYangProject1.DAL.Interfaces;
using OdomYangProject1.DAL.Repository;
using OdomYangProject1.Model;

namespace OdomYangProject1.Controller                 
{
    public class CustomerController                                                            
    {
        private readonly ICustomer customerRepo;

        public CustomerController()
        {
            this.customerRepo = new CustomerRepository();
        }

        public Customer GetCustomerByCustomerID(int id)
        {
            return this.customerRepo.GetByID(id);
        }

        public List<Customer> GetCustomerByName(String lastName, String firstName)
        {
            List<Customer> customers = new List<Customer>();
            customers = this.customerRepo.GetByName(lastName, firstName);
            return customers;
        }

        public List<Customer> GetCustomerByPhone(long phoneNumber)
        {
            List<Customer> customers = new List<Customer>();
            customers = customerRepo.GetByPhone(phoneNumber);
            return customers;
        }

        public Boolean DoesCustomerExist(int id)
        {
            Boolean doesExist = false;
            try
            {

                Customer customer = GetCustomerByCustomerID(id);
                if (customer.CustomerID != 0)
                {
                    doesExist = true;
                }
            }
            catch (Exception)
            {

                throw new Exception("Error");
            }

            return doesExist;
        }


        public void AddCustomer(List<string> customerInformation)
        {
            try
            {
                Customer customer = new Customer();
                customer.Fname = customerInformation[0];
                customer.Lname = customerInformation[1];
                customer.Phone = Convert.ToInt64(customerInformation[2]);
                customer.Ssn = Convert.ToInt32(customerInformation[3]);
                customer.Address = customerInformation[4];
                customer.Address2 = customerInformation[5];
                customer.City = customerInformation[6];
                customer.State = customerInformation[7];
                customer.Zip = Convert.ToInt32(customerInformation[8]);
                customer.Date_registered = DateTime.Now;
                this.customerRepo.Add(customer);
            }
            catch (Exception)
            {
                throw new Exception("Error");
            }
        }
    }
}
