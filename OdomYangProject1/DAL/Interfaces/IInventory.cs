﻿using OdomYangProject1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdomYangProject1.DAL.Interfaces
{
    public interface IInventory: IRepository<Inventory>
    {
        List<Inventory> FindInventoryByCategory(string category);
        List<Inventory> FindInventoryByStyle(string style);
        void DecrementStock(int id, int amount);
        void IncrementStock(int id, int amount);
    }
}
