﻿using OdomYangProject1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdomYangProject1.DAL.Interfaces
{
    public interface IRental:IRepository<Rental>
    {
        List<Inventory> GetInventoryByRentalID(int rentalID);
        Rental GetByID(int rentalID, int inventoryID);
    }
}
