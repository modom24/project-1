﻿using OdomYangProject1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdomYangProject1.DAL.Interfaces
{
    public interface IEmployee:IRepository<Employee>
    {
       Employee FindEmployeeByLogin(string username, string password);
    }
}
