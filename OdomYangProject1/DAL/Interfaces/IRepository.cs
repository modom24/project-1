﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdomYangProject1.DAL.Interfaces
{
   public interface IRepository<T>
    {
        IList<T> GetAll();

        T GetByID(int id);

        void Add(T entity);

        void Update();

        void Delete();
    }
}
