﻿using OdomYangProject1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdomYangProject1.DAL.Interfaces
{
    public interface IAdmin: IRepository<Admin>
    {
        Admin FindAdminByLogin(String username, String password);
        List<dynamic> QueryDB(String sql);
    }
}
