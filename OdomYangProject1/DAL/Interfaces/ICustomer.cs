﻿using OdomYangProject1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdomYangProject1.DAL.Interfaces
{
    public interface ICustomer:IRepository<Customer>
    {
        List<Customer> GetByName(String lastName, String firstName);
        List<Customer> GetByPhone(long phoneNumber);
    }
}
