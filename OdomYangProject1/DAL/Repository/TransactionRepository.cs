﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using OdomYangProject1.DAL.Interfaces;
using OdomYangProject1.Model;

namespace OdomYangProject1.DAL.Repository
{
    public class TransactionRepository: ITransaction
    {
        private readonly string connString;

        public TransactionRepository(string connString = "MySqlDBConnection")
        {
            this.connString = ConfigurationManager.ConnectionStrings[connString].ConnectionString;
        }
        
        public IList<Transaction> GetAll()
        {
            throw new NotImplementedException();
        }

        public Transaction GetByID(int id)
        {
            throw new NotImplementedException();
        }

        public List<Transaction> GetCustomerTransactions(int customerID)
        {
            List<Transaction> transactions = new List<Transaction>();
            try
            {
                using (MySqlConnection conn = new MySqlConnection(this.connString))
                {
                    string commandString = "SELECT * FROM TRANSACTIONS WHERE customerID = @customerID";
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(commandString, conn))
                    {
                        cmd.Parameters.AddWithValue("@customerID", customerID);
                        cmd.Parameters.AddWithValue("@isReturn", 0);
                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Transaction transaction = new Transaction();
                                transaction.TransactionID = reader["transactionID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["transactionID"]);
                                transaction.EmployeeID = reader["employeeID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["employeeID"]);
                                transaction.CustomerID = reader["customerID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["customerID"]);
                                transaction.DateTime = reader["datetime"] == DBNull.Value ? default(DateTime) : Convert.ToDateTime(reader["datetime"]);
                                transaction.Payment = reader["payment"] == DBNull.Value ? default(int) : Convert.ToDecimal(reader["payment"]);
                                transaction.IsReturn = reader["isReturn"] == DBNull.Value ? default(bool) : false;
                                transactions.Add(transaction);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return transactions;
        }



        public void Add(Transaction transaction)
        {
            try
            {
                using (MySqlConnection conn = new MySqlConnection(this.connString))
                {
                    const string cmdString = "INSERT INTO TRANSACTIONS(transactionID,employeeID,customerID,datetime,payment,isReturn) " +
                        "VALUES(@transactionID,@employeeID,@customerID,@datetime,@payment,@isReturn)";
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(cmdString, conn))
                    {
                        cmd.Parameters.AddWithValue("@transactionID", transaction.TransactionID);
                        cmd.Parameters.AddWithValue("@employeeID", transaction.EmployeeID);
                        cmd.Parameters.AddWithValue("@customerID", transaction.CustomerID);
                        var date = transaction.DateTime.ToString("yyyy-MM-dd H:mm:ss");
                        cmd.Parameters.AddWithValue("@datetime", date);
                        cmd.Parameters.AddWithValue("@payment", transaction.Payment);
                        cmd.Parameters.AddWithValue("@isReturn", transaction.IsReturn);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }



        public void Update()
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }

        public int findMaxId()
        {
            int max = 0;
            try {
                using(MySqlConnection conn = new MySqlConnection(this.connString))
                {
                    string connectionString = "SELECT MAX(transactionID) FROM TRANSACTIONS";
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(connectionString, conn)) {
                        if (cmd.ExecuteScalar() == DBNull.Value)
                        {
                            max = 0;
                        } else
                        {
                            max = Convert.ToInt32(cmd.ExecuteScalar());
                        }
                        
                      
                    }
                }
            }
            catch(Exception) {
                throw;
            }
            return max;
        }

       
    }
}
