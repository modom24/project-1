﻿using OdomYangProject1.DAL.Interfaces;
using OdomYangProject1.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using MySql.Data.MySqlClient;

namespace OdomYangProject1.DAL.Repository
{

    /// <summary>
    /// Employee Repository class
    /// </summary>
    public class EmployeeRepository: IEmployee
    {
        private readonly string connString;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeRepository"/> class.
        /// </summary>
        /// <param name="connString">The connection string.</param>
        public EmployeeRepository(string connString = "MySqlDBConnection") {
            this.connString = ConfigurationManager.ConnectionStrings[connString].ConnectionString;
        }


        /// <summary>
        /// Finds the employee by username and password.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns> a Employee </returns>
        public Employee FindEmployeeByLogin(string username, string password) {
            Employee employee = new Employee();
            try
            {
                using (MySqlConnection conn = new MySqlConnection(this.connString)) {
                    string commandString = "SELECT * FROM EMPLOYEE WHERE username = @username AND password = @password";
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(commandString, conn)) {
                        cmd.Parameters.AddWithValue("@username", username);
                        cmd.Parameters.AddWithValue("@password", password);
                        using (MySqlDataReader reader = cmd.ExecuteReader()) {
                            while (reader.Read()) {

                                employee.EmployeeID = reader["employeeID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["employeeID"]);
                                employee.Fname = reader["fname"].ToString();
                                employee.Lname = reader["lname"].ToString();
                                employee.Phone = reader["phone"] == DBNull.Value ? default(long) : Convert.ToInt64(reader["phone"]);
                                employee.Ssn = reader["ssn"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["ssn"]);
                                employee.Address = reader["address"].ToString();
                                employee.Address2 = reader["address2"].ToString();
                                employee.City = reader["city"].ToString();
                                employee.Zip = reader["zip"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["zip"]);
                                employee.State = reader["state"].ToString();
                                employee.Username = reader["username"].ToString();
                                employee.Password = reader["password"].ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception) {
                throw;
            }
            return employee;
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public IList<Employee> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Employee GetByID(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Adds the specified employee.
        /// </summary>
        /// <param name="employee">The employee.</param>
        /// <exception cref="NotImplementedException"></exception>
        public void Add(Employee employee)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Updates this instance.
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        public void Update()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Deletes this instance.
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        public void Delete()
        {
            throw new NotImplementedException();
        }

       
    }
}
