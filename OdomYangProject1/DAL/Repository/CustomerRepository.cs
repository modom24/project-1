﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using OdomYangProject1.DAL.Interfaces;
using OdomYangProject1.Model;
using System.Configuration;


namespace OdomYangProject1.DAL.Repository
{

    /// <summary>
    /// Customer Repository DAL class
    /// </summary>
    public class CustomerRepository : ICustomer
    {

        private readonly string connString;


        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerRepository"/> class.
        /// </summary>
        /// <param name="connString">The connection string.</param>
        public CustomerRepository(string connString = "MySqlDBConnection")
        {
            this.connString = ConfigurationManager.ConnectionStrings[connString].ConnectionString;
        }


        /// <summary>
        /// Gets all customers in DB
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public IList<Customer> GetAll()
        {
            throw new NotImplementedException();
        }

        public List<Customer> GetByPhone(long phoneNumber)
        {

            List<Customer> customers = new List<Customer>();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(this.connString))
                {
                    string commandString = "SELECT * FROM CUSTOMER WHERE phone = @phone";
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(commandString, conn))
                    {
                        cmd.Parameters.AddWithValue("@phone", phoneNumber);
                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                customer.CustomerID = reader["customerID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["customerID"]);
                                customer.Fname = reader["fname"].ToString();
                                customer.Lname = reader["lname"].ToString();
                                customer.Phone = reader["phone"] == DBNull.Value ? default(long) : Convert.ToInt64(reader["phone"]);
                                customer.Ssn = reader["ssn"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["ssn"]);
                                customer.Address = reader["address"].ToString();
                                customer.Address2 = reader["address2"].ToString();
                                customer.City = reader["city"].ToString();
                                customer.Zip = reader["zip"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["zip"]);
                                customer.State = reader["state"].ToString();
                                customer.Date_registered = reader["date_registered"] == DBNull.Value ? default(DateTime) : Convert.ToDateTime(reader["date_registered"]);
                                customers.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return customers;
        }

        public List<Customer> GetByName(String lastName, String firstName)
        {
            List<Customer> customers = new List<Customer>();

            try
            {
                using (MySqlConnection conn = new MySqlConnection(this.connString))
                {
                    string commandString = "SELECT * FROM CUSTOMER WHERE fname = @fname AND lname = @lname";
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(commandString, conn))
                    {
                        cmd.Parameters.AddWithValue("@fname", firstName);
                        cmd.Parameters.AddWithValue("@lname", lastName);
                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                customer.CustomerID = reader["customerID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["customerID"]);
                                customer.Fname = reader["fname"].ToString();
                                customer.Lname = reader["lname"].ToString();
                                customer.Phone = reader["phone"] == DBNull.Value ? default(long) : Convert.ToInt64(reader["phone"]);
                                customer.Ssn = reader["ssn"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["ssn"]);
                                customer.Address = reader["address"].ToString();
                                customer.Address2 = reader["address2"].ToString();
                                customer.City = reader["city"].ToString();
                                customer.Zip = reader["zip"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["zip"]);
                                customer.State = reader["state"].ToString();
                                customer.Date_registered = reader["date_registered"] == DBNull.Value ? default(DateTime) : Convert.ToDateTime(reader["date_registered"]);
                                customers.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return customers;
        }


        /// <summary>
        /// Gets the customers by ID
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public Customer GetByID(int id)
        {
            Customer customer = new Customer();
            try
            {
                using (MySqlConnection conn = new MySqlConnection(this.connString))
                {
                    string commandString = "SELECT * FROM CUSTOMER WHERE customerID = @customerID";
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(commandString, conn))
                    {
                        cmd.Parameters.AddWithValue("@customerID", id);
                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerID = reader["customerID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["customerID"]);
                                customer.Fname = reader["fname"].ToString();
                                customer.Lname = reader["lname"].ToString();
                                customer.Phone = reader["phone"] == DBNull.Value ? default(long) : Convert.ToInt64(reader["phone"]);
                                customer.Ssn = reader["ssn"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["ssn"]);
                                customer.Address = reader["address"].ToString();
                                customer.Address2 = reader["address2"].ToString();
                                customer.City = reader["city"].ToString();
                                customer.Zip = reader["zip"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["zip"]);
                                customer.State = reader["state"].ToString();
                                customer.Date_registered = reader["date_registered"] == DBNull.Value ? default(DateTime) : Convert.ToDateTime(reader["date_registered"]);

                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return customer;
        }


        /// <summary>
        /// Adds a specified customer to the DB.
        /// </summary>
        /// <param name="customer">The customer.</param>
        public void Add(Customer customer)
        {
            try
            {
                using (MySqlConnection conn = new MySqlConnection(this.connString))
                {
                    const string cmdString = "INSERT INTO CUSTOMER(customerID,fname,lname,phone,ssn,date_registered,address,address2,zip,city,state) " +
                        "VALUES(@customerID,@fname,@lname,@phone,@ssn,@date_registered,@address,@address2,@zip,@city,@state)";
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(cmdString, conn))
                    {
                        cmd.Parameters.AddWithValue("@customerID", customer.CustomerID);
                        cmd.Parameters.AddWithValue("@fname", customer.Fname);
                        cmd.Parameters.AddWithValue("@lname", customer.Lname);
                        cmd.Parameters.AddWithValue("@phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@ssn", customer.Ssn);
                        var dateTime = customer.Date_registered.ToString("yyyy-MM-dd H:mm:ss");
                        cmd.Parameters.AddWithValue("@date_registered", dateTime);
                        cmd.Parameters.AddWithValue("@address", customer.Address);
                        cmd.Parameters.AddWithValue("@address2", customer.Address2);
                        cmd.Parameters.AddWithValue("@zip", customer.Zip);
                        cmd.Parameters.AddWithValue("@city", customer.City);
                        cmd.Parameters.AddWithValue("@state", customer.State);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        public void Update()
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }
    }
}