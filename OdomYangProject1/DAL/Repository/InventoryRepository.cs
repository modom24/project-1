﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using OdomYangProject1.DAL.Interfaces;
using OdomYangProject1.Model;
using System.Configuration;

namespace OdomYangProject1.DAL.Repository
{
    /// <summary>
    /// Inventory Repository DAL class
    /// </summary>
    public class InventoryRepository: IInventory
    {
        private readonly string connString;

        /// <summary>
        /// Initializes a new instance of the <see cref="InventoryRepository"/> class.
        /// </summary>
        /// <param name="connString">The connection string.</param>
        public InventoryRepository(string connString = "MySqlDBConnection")
        {
            this.connString = ConfigurationManager.ConnectionStrings[connString].ConnectionString;
        }

        /// <summary>
        /// Finds the inventory by category.
        /// </summary>
        /// <param name="category">The category.</param>
        /// <returns>a collection of inventory filtered by category</returns>
        public List<Inventory> FindInventoryByCategory(string category)
        {
            List<Inventory> items = new List<Inventory>();
            try
            {
                using (MySqlConnection conn = new MySqlConnection(this.connString))
                {
                    string commandString = "SELECT * FROM INVENTORY WHERE category = @category";
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(commandString, conn))
                    {
                        cmd.Parameters.AddWithValue("@category", category);
                       
                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Inventory item = new Inventory();
                                item.InventoryId = reader["inventoryID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["inventoryID"]);
                                item.ModelName = reader["model_name"].ToString();
                                item.Category = reader["category"].ToString();
                                item.RentalFee = reader["rental_fee"] == DBNull.Value ? default(int) : Convert.ToDecimal(reader["rental_fee"]);
                                item.FineRate = reader["fine_rate"] == DBNull.Value ? default(int) : Convert.ToDecimal(reader["fine_rate"]);
                                item.Color = reader["color"].ToString();
                                item.Style = reader["style"].ToString();
                                item.Stock = reader["stock"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["stock"]);
                                items.Add(item);
                             
                            }

                        }
                    }
                }
            }
            catch (Exception) {
                throw;
            }
            return items;
        }

        /// <summary>
        /// Finds the inventory by style.
        /// </summary>
        /// <param name="style">The style.</param>
        /// <returns> a collection of inventory filtered by style</returns>
        public List<Inventory> FindInventoryByStyle(string style)
        {
            List<Inventory> items = new List<Inventory>();
            try
            {
                using (MySqlConnection conn = new MySqlConnection(this.connString))
                {
                    string commandString = "SELECT * FROM INVENTORY WHERE style = @style";
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(commandString, conn))
                    {
                        cmd.Parameters.AddWithValue("@style", style);

                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Inventory item = new Inventory();
                                item.InventoryId = reader["inventoryID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["inventoryID"]);
                                item.ModelName = reader["model_name"].ToString();
                                item.Category = reader["category"].ToString();
                                item.RentalFee = reader["rental_fee"] == DBNull.Value ? default(int) : Convert.ToDecimal(reader["rental_fee"]);
                                item.FineRate = reader["fine_rate"] == DBNull.Value ? default(int) : Convert.ToDecimal(reader["fine_rate"]);
                                item.Color = reader["color"].ToString();
                                item.Style = reader["style"].ToString();
                                item.Stock = reader["stock"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["stock"]);
                                items.Add(item);

                            }

                        }
                    }
                }
            }
            catch (Exception) {
                throw;
            }
            return items;
        }

        public IList<Inventory> GetAll()
        {
            List<Inventory> items = new List<Inventory>();
            try
            {
                using (MySqlConnection conn = new MySqlConnection(this.connString))
                {
                    string commandString = "SELECT * FROM INVENTORY";
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(commandString, conn))
                    {
                        

                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Inventory item = new Inventory();
                                item.InventoryId = reader["inventoryID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["inventoryID"]);
                                item.ModelName = reader["model_name"].ToString();
                                item.Category = reader["category"].ToString();
                                item.RentalFee = reader["rental_fee"] == DBNull.Value ? default(int) : Convert.ToDecimal(reader["rental_fee"]);
                                item.FineRate = reader["fine_rate"] == DBNull.Value ? default(int) : Convert.ToDecimal(reader["fine_rate"]);
                                item.Color = reader["color"].ToString();
                                item.Style = reader["style"].ToString();
                                item.Stock = reader["stock"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["stock"]);
                                items.Add(item);

                            }

                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return items;
        }

        public Inventory GetByID(int id)
        {
            Inventory item = new Inventory();
            try
            {
                using (MySqlConnection conn = new MySqlConnection(this.connString))
                {
                    string commandString = "SELECT * FROM INVENTORY WHERE inventoryID = @inventoryID";
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(commandString, conn))
                    {
                        cmd.Parameters.AddWithValue("@inventoryID", id);

                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                               
                                item.InventoryId = reader["inventoryID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["inventoryID"]);
                                item.ModelName = reader["model_name"].ToString();
                                item.Category = reader["category"].ToString();
                                item.RentalFee = reader["rental_fee"] == DBNull.Value ? default(int) : Convert.ToDecimal(reader["rental_fee"]);
                                item.FineRate = reader["fine_rate"] == DBNull.Value ? default(int) : Convert.ToDecimal(reader["fine_rate"]);
                                item.Color = reader["color"].ToString();
                                item.Style = reader["style"].ToString();
                                item.Stock = reader["stock"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["stock"]);
                                

                            }

                        }
                    }
                }
            }
            catch (Exception) {
                throw;
            }
            return item;
        }

        public void DecrementStock(int id, int amount)
        {
            try
            {
                using (MySqlConnection conn = new MySqlConnection(this.connString))
                {
                    string commandString = "UPDATE INVENTORY SET stock=(stock-@amount) WHERE inventoryID = @inventoryID";
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(commandString,conn))
                    {
                        cmd.Parameters.AddWithValue("@amount", amount);
                        cmd.Parameters.AddWithValue("inventoryID", id);
                        cmd.ExecuteNonQuery();
                    }
                    
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        public void IncrementStock(int id, int amount)
        {
            try
            {
                using (MySqlConnection conn = new MySqlConnection(this.connString))
                {
                    string commandString = "UPDATE INVENTORY SET stock=(stock+@amount) WHERE inventoryID = @inventoryID";
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(commandString, conn))
                    {
                        cmd.Parameters.AddWithValue("@amount", amount);
                        cmd.Parameters.AddWithValue("inventoryID", id);
                        cmd.ExecuteNonQuery();
                    }

                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Add(Inventory entity)
        {
            throw new NotImplementedException();
        }

        public void Update()
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }
    }
}
