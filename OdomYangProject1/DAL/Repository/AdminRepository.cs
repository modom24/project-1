﻿using OdomYangProject1.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OdomYangProject1.Model;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.Dynamic;

namespace OdomYangProject1.DAL.Repository
{
    class AdminRepository : IAdmin
    {

        private readonly string connString;

        public AdminRepository(string connString = "MySqlDBConnection")
        {
            this.connString = ConfigurationManager.ConnectionStrings[connString].ConnectionString;
        }


        public Admin FindAdminByLogin(string username, string password)
        {
            Admin admin = new Admin();
            try
            {
                using (MySqlConnection conn = new MySqlConnection(this.connString))
                {
                    string commandString = "SELECT * FROM ADMIN WHERE admin_username = @admin_username AND admin_password = @admin_password";
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(commandString, conn))
                    {
                        cmd.Parameters.AddWithValue("@admin_username", username);
                        cmd.Parameters.AddWithValue("@admin_password", password);
                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                admin.AdminID = reader["adminID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["adminID"]);
                                admin.Fname = reader["fname"].ToString();
                                admin.Lname = reader["lname"].ToString();
                                admin.Phone = reader["phone"] == DBNull.Value ? default(long) : Convert.ToInt64(reader["phone"]);
                                admin.Ssn = reader["ssn"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["ssn"]);
                                admin.Admin_Username = reader["admin_username"].ToString();
                                admin.Admin_Password = reader["admin_password"].ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return admin;
        }


        public List<dynamic> QueryDB(String sql)
        {
            var list = new List<dynamic>();
            try
            {
                using (MySqlConnection conn = new MySqlConnection(this.connString))
                {
                    string commandString = sql;
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(commandString, conn))
                    {
                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var obj = new ExpandoObject();
                                var d = obj as IDictionary<String, object>;
                                for (int index = 0; index < reader.FieldCount; index++)
                                    d[reader.GetName(index)] = reader.GetString(index);

                                list.Add(obj);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return list;
        }



        public void Add(Admin entity)
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }

        public IList<Admin> GetAll()
        {
            throw new NotImplementedException();
        }

        public Admin GetByID(int id)
        {
            throw new NotImplementedException();
        }

        public void Update()
        {
            throw new NotImplementedException();
        }

       
    }
}
