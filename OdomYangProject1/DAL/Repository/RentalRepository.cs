﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using OdomYangProject1.DAL.Interfaces;
using OdomYangProject1.Model;

namespace OdomYangProject1.DAL.Repository
{
    public class RentalRepository: IRental
    {
        private readonly string connString;

        public RentalRepository(string connString = "MySqlDBConnection")
        {
            this.connString = ConfigurationManager.ConnectionStrings[connString].ConnectionString;
        }

        public IList<Rental> GetAll()
        {
            List<Rental> rentals = new List<Rental>();
            try
            {
                using (MySqlConnection conn = new MySqlConnection(this.connString))
                {
                    string commandString = "SELECT * FROM RENTALS";
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(commandString, conn))
                    {
                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Rental rental = new Rental();
                                rental.RentalTransactionID = reader["rentalTransactionID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["rentalTransactionID"]);
                                rental.InventoryID = reader["inventoryID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["inventoryID"]);
                                rental.DueDate = reader["due_date"] == DBNull.Value ? default(DateTime) : Convert.ToDateTime(reader["due_date"]);
                                rental.Qty = reader["quantity"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["quantity"]);
                                rentals.Add(rental);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return rentals;
        }

        public List<Inventory>  GetInventoryByRentalID(int rentalID)
        {
            List<Inventory> items = new List<Inventory>();
            try
            {
                using (MySqlConnection conn = new MySqlConnection(this.connString))
                {
                    string commandString = "SELECT inventoryID FROM RENTALS WHERE rentalTransactionID = @rentalTransactionID";
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(commandString, conn))
                    {
                        cmd.Parameters.AddWithValue("@rentalTransactionID", rentalID);
                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Inventory item = new Inventory();
                                item.InventoryId = reader["inventoryID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["inventoryID"]);
                                items.Add(item);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return items;
        }

        public Rental GetByID(int id)
        {
            throw new NotImplementedException();
        }



        public void Add(Rental rental)
        {
            try
            {
                using (MySqlConnection conn = new MySqlConnection(this.connString))
                {
                    const string cmdString = "INSERT INTO RENTALS(rentalTransactionID,inventoryID,due_date,quantity) " +
                        "VALUES(@rentalTransactionID,@inventoryID,@due_date,@quantity)";
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(cmdString, conn))
                    {
                        cmd.Parameters.AddWithValue("@rentalTransactionID", rental.RentalTransactionID);
                        cmd.Parameters.AddWithValue("@inventoryID", rental.InventoryID);
                        var duedate = rental.DueDate.ToString("yyyy-MM-dd H:mm:ss");
                        cmd.Parameters.AddWithValue("@due_date", duedate);
                        cmd.Parameters.AddWithValue("@quantity", rental.Qty);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update()
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }

        public Rental GetByID(int rentalID, int inventoryID)
        {
            Rental rental = new Rental();
            try
            {
                using (MySqlConnection conn = new MySqlConnection(this.connString))
                {
                    string commandString = "SELECT * FROM RENTALS WHERE rentalTransactionID = @rentalTransactionID AND inventoryID = @inventoryID";
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(commandString, conn))
                    {
                        cmd.Parameters.AddWithValue("@rentalTransactionID", rentalID);
                        cmd.Parameters.AddWithValue("@inventoryID", inventoryID);
                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                rental.RentalTransactionID = reader["rentalTransactionID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["rentalTransactionID"]);
                                rental.InventoryID = reader["inventoryID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["inventoryID"]);
                                rental.DueDate = reader["due_date"] == DBNull.Value ? default(DateTime) : Convert.ToDateTime(reader["due_date"]);
                                rental.Qty = reader["quantity"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["quantity"]);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return rental;
        }
    }
}
