﻿using MySql.Data.MySqlClient;
using OdomYangProject1.DAL.Interfaces;
using OdomYangProject1.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdomYangProject1.DAL.Repository
{
    public class ReturnRepository: IReturn
    {
        private readonly string connString;

        public ReturnRepository(string connString = "MySqlDBConnection")
        {
            this.connString = ConfigurationManager.ConnectionStrings[connString].ConnectionString;
        }

        public IList<Return> GetAll()
        {
            List<Return> returns = new List<Return>();
            try
            {
                using (MySqlConnection conn = new MySqlConnection(this.connString))
                {
                    string commandString = "SELECT * FROM RETURNS";
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(commandString, conn))
                    {
                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Return returnItem = new Return();
                                returnItem.ReturnTransactionID = reader["returnTransactionID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["returnTransactionID"]);
                                returnItem.RentalTransactionID = reader["rentalTransactionID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["rentalTransactionID"]);
                                returnItem.InventoryID = reader["inventoryID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["inventoryID"]);
                                returnItem.Quantity_Returned = reader["inventoryID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["inventoryID"]);
                                returns.Add(returnItem);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return returns;
        }

        public Return GetByID(int id)
        {
            throw new NotImplementedException();
        }

        
        
        public List<Return> GetReturnByRentalID(int id, int inventoryID)
        {
            List<Return> returns = new List<Return>();
            try
            {
                using (MySqlConnection conn = new MySqlConnection(this.connString))
                {
                    string commandString = "SELECT * FROM RETURNS WHERE rentalTransactionID = @rentalTransactionID AND inventoryID = @inventoryID";
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(commandString, conn))
                    {
                        cmd.Parameters.AddWithValue("@rentalTransactionID", id);
                        cmd.Parameters.AddWithValue("@inventoryId", inventoryID);
                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Return returnItem = new Return();
                                returnItem.ReturnTransactionID = reader["returnTransactionID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["returnTransactionID"]);
                                returnItem.RentalTransactionID = reader["rentalTransactionID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["rentalTransactionID"]);
                                returnItem.InventoryID = reader["inventoryID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["inventoryID"]);
                                returnItem.Quantity_Returned = reader["inventoryID"] == DBNull.Value ? default(int) : Convert.ToInt32(reader["inventoryID"]);
                                returns.Add(returnItem);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return returns;
        }

        public void Add(Return returningItem)
        {
            try
            {
                using (MySqlConnection conn = new MySqlConnection(this.connString))
                {
                    const string cmdString = "INSERT INTO RETURNS(returnTransactionID, rentalTransactionID, inventoryID, quantity_returned) " +
                        "VALUES(@returnTransactionID, @rentalTransactionID, @inventoryID,@quantity_returned)";
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(cmdString, conn))
                    {
                        cmd.Parameters.AddWithValue("@returnTransactionID", returningItem.ReturnTransactionID);
                        cmd.Parameters.AddWithValue("@rentalTransactionID", returningItem.RentalTransactionID);
                        cmd.Parameters.AddWithValue("@inventoryID", returningItem.InventoryID);
                        cmd.Parameters.AddWithValue("@quantity_returned", returningItem.Quantity_Returned);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update()
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }

        public List<Return> GetReturnByRentalID(int id)
        {
            throw new NotImplementedException();
        }
    }
}
