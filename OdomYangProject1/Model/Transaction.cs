﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdomYangProject1.Model
{
    public class Transaction
    {
        public int TransactionID { get; set; }
        public int EmployeeID { get; set; }
        public int CustomerID { get; set; }
        public DateTime DateTime { get; set; }
        public decimal Payment { get; set; }
        public Boolean IsReturn { get; set; }
        public Transaction() { }
    }
}
