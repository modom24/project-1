﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdomYangProject1.Model
{
    public class Rental
    {
        public int RentalTransactionID { get; set; }
        public int InventoryID { get; set; }
        public DateTime DueDate { get; set; }
        public int Qty { get; set; }
       

        public Rental()
        {
        }
    }
}
