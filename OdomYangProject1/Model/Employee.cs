﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdomYangProject1.Model
{

    /// <summary>
    /// Represents an employee from the database
    /// </summary>
   public class Employee
    {
       public int EmployeeID { get; set;}
       public string Fname { get; set; }
       public string Lname { get; set; }
       public long Phone { get; set; }
       public int Ssn { get; set; }
       public string Address { get; set; }
       public string Address2 { get; set;}
       public string City { get; set; }
       public int Zip { get; set; }
       public string State { get; set;}
       public string Username { get; set; }
       public string Password { get; set;}
       public Employee(){}
    }
}
