﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdomYangProject1.Model
{
    public class Return
    {
        public int ReturnTransactionID { get; set; }
        public int RentalTransactionID { get; set; }
        public int InventoryID { get; set; }
        public int Quantity_Returned { get; set; }
        public Return()
        {

        }
    }
}
