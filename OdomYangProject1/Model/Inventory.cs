﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdomYangProject1.Model
{
   public class Inventory
   {

        /// <summary>
        /// Represents furniture and appliances from the database
        /// </summary>
       public int InventoryId { get; set; }
       public string ModelName { get; set; }
       public int Stock { get; set; }
       public decimal RentalFee { get; set; }
       public string Category { get; set; }
       public string Style { get; set; }
       public string Color { get; set; }
       public decimal FineRate { get; set; }
       public Inventory()
       {
       }

   }
}
