﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdomYangProject1.Model
{
    

    public class Admin
    {
        public int AdminID { get; set; }
        public String Fname { get; set; }
        public String Lname { get; set; }
        public long Phone { get; set; }
        public int Ssn { get; set; }
        public String Admin_Username { get; set; }
        public String Admin_Password { get; set; }

        public Admin()
        {

        }

    }
}
