﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdomYangProject1.Model
{

    /// <summary>
    /// Represents a customer from the database
    /// </summary>
  public class Customer
    {
      public int CustomerID { get; set; }
      public string Fname { get; set; }
      public string Lname { get; set; }
      public int Ssn { get; set; }
      public DateTime Date_registered { get; set;}
      public long Phone { get; set; }
      public string Address { get; set; }
      public string Address2 { get; set; }
      public int Zip { get; set;}
      public string City {get; set;}
      public string State { get; set; }
      public Customer(){}
    }
}
