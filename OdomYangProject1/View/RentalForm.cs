﻿using OdomYangProject1.Controller;
using OdomYangProject1.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Windows.Forms;

namespace OdomYangProject1.View
{
    public partial class RentalForm : Form
    {
        private List<Inventory> itemsToRent;
        private List<Rental> rentals;
        private decimal rentalTotal;
        private CustomerController customerController;
        private Customer currentCustomer;
        private TransactionController transactionController;
        private InventoryController inventoryController;
        private Employee currentEmployee;
        private Boolean isLoggedIn;
        private BindingSource bindingSource;
        private List<dynamic> rentalInfo;
        private dynamic currentInfo;
        private DataTable display;
        public RentalForm(List<Inventory> itemsToRent, Employee currentEmployee)
        {
            this.itemsToRent = itemsToRent;
            this.rentals = new List<Rental>();
            this.rentalInfo = new List<dynamic>();
            this.customerController = new CustomerController();
            this.transactionController = new TransactionController();
            this.inventoryController = new InventoryController();
            this.currentEmployee = currentEmployee;
            InitializeComponent();
            this.totalCostTextbox.Enabled = false;
            this.completeRentalButton.Enabled = false;
            isLoggedIn = false;
            this.dataGridView1.AllowUserToAddRows = false;
            this.rentalTotal = 0;
}

        public void addRental(Inventory item, Rental rental)
        {
            if(this.isLoggedIn == true && rental.Qty<=item.Stock)
            {
                try
                {


                    this.dataGridView1.Visible = true;
                    this.dataGridView1.Refresh();
                    this.rentalTotal += item.RentalFee * rental.Qty;
                    rental.InventoryID = item.InventoryId;
                    rental.DueDate = dateTimePicker1.Value;
                    this.itemsToRent.Add(item);
                    this.rentals.Add(rental);
                    dynamic info = new ExpandoObject();
                    info.Name = item.ModelName;
                    info.InventoryId = rental.InventoryID;
                    info.DueDate = rental.DueDate;
                    info.Quantity = rental.Qty;
                    this.rentalInfo.Add(info);
                    this.display = ToDataTable(this.rentalInfo);
                    this.totalCostTextbox.Text = "$" + rentalTotal;
                    this.dataGridView1.DataSource = this.bindingSource;
                    this.dataGridView1.DataSource = display;
                    this.completeRentalButton.Enabled = true;
                    

                }
                catch (Exception)
                {

                    MessageBox.Show("error adding transaction");
                }

            }
            else if(this.isLoggedIn == false)
            {
                MessageBox.Show("Invalid customer credentials");
            }
            else
            {
                MessageBox.Show("Invalid rental qty. Not enough in stock");
            }
        }

        private DataTable ToDataTable(IEnumerable<dynamic> items)
        {
            var data = items.ToArray();
            if (data.Count() == 0) return null;

            var dt = new DataTable();
            foreach (var key in ((IDictionary<string, object>)data[0]).Keys)
            {
                
                    dt.Columns.Add(key);
                
                
            }
            foreach (var d in data)
            {
                
                    dt.Rows.Add(((IDictionary<string, object>)d).Values.ToArray());
               
            }
            return dt;
        }





        private void completeRentalButton_Click(object sender, EventArgs e)
        {
            try
            {

                int id = this.transactionController.findMaxTransId() + 1;
                Transaction current = new Transaction();
                current.TransactionID = id;
                current.EmployeeID = this.currentEmployee.EmployeeID;
                current.CustomerID = this.currentCustomer.CustomerID;
                current.DateTime = DateTime.Today;
                current.Payment = this.rentalTotal;
                this.transactionController.addRental(current, this.rentals);
                foreach (Rental aRental in this.rentals)
                {
                    this.inventoryController.DecrementInventory(aRental.InventoryID, aRental.Qty);
                }
                MessageBox.Show("Successful Purchase");
                this.Close();
            }
            catch (Exception)
            {

                MessageBox.Show("Error adding rental");
            }
           
        }

        private Boolean verifyCustomer(int id)
        {
            return this.customerController.DoesCustomerExist(id);
        }

        private void customerLoginButton_Click(object sender, EventArgs e)
        {
            int custID = -1;
            if (this.customerIDTextbox.Text.All(char.IsDigit) && !String.IsNullOrEmpty(this.customerIDTextbox.Text))
            {
                custID = Convert.ToInt32(this.customerIDTextbox.Text);
                this.isLoggedIn = verifyCustomer(custID);
                if(this.isLoggedIn == true)
                {
                    this.currentCustomer = this.customerController.GetCustomerByCustomerID(custID);
                    this.customerIDTextbox.Clear();
                    this.customerIDTextbox.Enabled = false;
                    this.customerLoginButton.Enabled = false;
                    this.Text = "Currently serving " + this.currentCustomer.Fname + "!";
                }
                else
                {
                    MessageBox.Show("Incorrect Customer Credentials");
                }
            }
            else
            {
                MessageBox.Show("CustomerID cannot be empty and must be a number");
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            this.rentals.Last().DueDate = this.dateTimePicker1.Value;
            this.rentalInfo.Last().DueDate = this.dateTimePicker1.Value;
            this.display = ToDataTable(this.rentalInfo);
            this.dataGridView1.DataSource = this.display;
        }

        private void update_Click(object sender, EventArgs e)
        {
            this.rentalInfo = new List<dynamic>();
            int i = 0;
            int last = this.rentals.Count;
            this.rentals.RemoveAt(last-1);
            this.itemsToRent.RemoveAt(last-1);
            foreach (var rental in this.rentals)
            {
                dynamic info = new ExpandoObject();
                info.Name = this.itemsToRent[i].ModelName;
                info.InventoryId = rental.InventoryID;
                info.DueDate = rental.DueDate;
                info.Quantity = rental.Qty;
                this.rentalInfo.Add(info);
            }

            DataTable db = this.ToDataTable(this.rentalInfo);
            this.dataGridView1.DataSource = db;

        }
           
    }
}
