﻿using OdomYangProject1.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace OdomYangProject1.View
{
    public partial class RegistrationForm : Form
    {
        private readonly CustomerController customerController;
        public RegistrationForm()
        {
            this.customerController = new CustomerController();
            InitializeComponent();
        }

        private void registerCustomerButton_Click(object sender, EventArgs e)
        {
            try
            {

                if (!verifyNumericInput(this.customerPhoneTextbox.Text) || this.customerPhoneTextbox.Text.Length != 10)
                {
                    MessageBox.Show("Phone Number may only contain numbers and must be 10 digits long");
                }
                else if (!verifyNumericInput(this.customerSSNTextbox.Text) || this.customerSSNTextbox.Text.Length != 9)
                {
                    MessageBox.Show("SSN may only contain numbers and must be 9 digits long");
                }
                else if (!verifyNumericInput(this.customerZipTextbox.Text) || this.customerZipTextbox.Text.Length != 5)
                {
                    MessageBox.Show("Zip may only contain numbers and must be 5 digits long");
                }
                else if (checkNullEntry())
                {
                    List<string> customerInformation = new List<string>();
                    customerInformation.Add(this.customerFnameTextbox.Text);
                    customerInformation.Add(this.customerLnameTextbox.Text);
                    customerInformation.Add(this.customerPhoneTextbox.Text);
                    customerInformation.Add(this.customerSSNTextbox.Text);
                    customerInformation.Add(this.customerAddressTextbox.Text);
                    customerInformation.Add(this.customerAddressTwoTextbox.Text);
                    customerInformation.Add(this.customerCityTextbox.Text);
                    customerInformation.Add(this.customerStateComboBox.Text);
                    customerInformation.Add(this.customerZipTextbox.Text);
                    this.customerController.AddCustomer(customerInformation);
                    MessageBox.Show("Customer Registered", "Success!");
                    this.Close();

                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error adding customer to database");
            }
        }

        private Boolean checkNullEntry()
        {
            if (string.IsNullOrWhiteSpace(this.customerFnameTextbox.Text))
            {
                MessageBox.Show("First Name cannot be empty");
            }
            else if (string.IsNullOrWhiteSpace(this.customerLnameTextbox.Text))
            {
                MessageBox.Show("Last Name cannot be empty");
            }
            else if (string.IsNullOrWhiteSpace(this.customerPhoneTextbox.Text))
            {
                MessageBox.Show("Phone Number cannot be empty");
            }
            else if (string.IsNullOrWhiteSpace(this.customerSSNTextbox.Text))
            {
                MessageBox.Show("SSN cannot be empty");
            }
            else if (string.IsNullOrWhiteSpace(this.customerAddressTextbox.Text))
            {
                MessageBox.Show("Address cannot be empty");
            }
            else if (string.IsNullOrWhiteSpace(this.customerCityTextbox.Text))
            {
                MessageBox.Show("City cannot be empty");
            }
            else if (string.IsNullOrWhiteSpace(this.customerStateComboBox.Text))
            {
                MessageBox.Show("State cannot be empty");
            }
            else if (string.IsNullOrWhiteSpace(this.customerZipTextbox.Text))
            {
                MessageBox.Show("Zip cannot be empty");
            }
            else
            {
                return true;
            }
            return false;
        }

        private Boolean verifyNumericInput(string userInput)
        {
            if (userInput.All(char.IsDigit))
             {
                return true;
             }

            return false;
        }
    }
}
