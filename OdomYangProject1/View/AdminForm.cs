﻿using OdomYangProject1.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OdomYangProject1.View
{
    public partial class AdminForm : Form
    {
        AdminController adminController;
        public AdminForm()
        {
            InitializeComponent();
             this.adminController = new AdminController();
        }

        private void queryButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.dataGridView1.DataSource = ToDataTable(this.adminController.QueryDB(this.queryTextbox.Text));
            }
            catch (Exception)
            {

                MessageBox.Show("Error with Query");
            }
        }

        private DataTable ToDataTable(IEnumerable<dynamic> items)
        {
            var data = items.ToArray();
            if (data.Count() == 0) return null;

            var dt = new DataTable();
            foreach (var key in ((IDictionary<string, object>)data[0]).Keys)
            {

                dt.Columns.Add(key);

            }
            foreach (var d in data)
            {

                dt.Rows.Add(((IDictionary<string, object>)d).Values.ToArray());

            }
            return dt;
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            this.queryTextbox.Clear();
        }
    }
}
