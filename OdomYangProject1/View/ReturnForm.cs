﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OdomYangProject1.Controller;
using OdomYangProject1.Model;
using System.Dynamic;

namespace OdomYangProject1.View
{
    public partial class ReturnForm : Form
    {

        private List<Return> returns;
        private TransactionController tranCon;
        private Customer currCustomer;
        private Employee currEmployee;
        private InventoryController invenCon;
        private List<dynamic> returnDisplay;
        private List<Rental> rentals;
        private List<Inventory> items;
        public ReturnForm(Customer currCustomer, Employee currEmployee)
        {

            this.currCustomer = currCustomer;
            this.returns = new List<Return>();
            this.tranCon = new TransactionController();
            this.invenCon = new InventoryController();
            this.rentals = new List<Rental>();
            this.items = new List<Inventory>();
            this.returnDisplay = new List<dynamic>();
            this.currEmployee = currEmployee;
            InitializeComponent();
        }

        public void AddReturns(Rental rental, int qty)
        {
            this.rentals.Add(rental);
            List<Inventory> items = this.invenCon.SortInventoryByCriteria("All", "");
            Return aReturn = new Return();
            aReturn.InventoryID = rental.InventoryID;
            aReturn.Quantity_Returned = qty;
            aReturn.ReturnTransactionID = this.tranCon.findMaxTransId()+1;
            aReturn.RentalTransactionID = rental.RentalTransactionID;
            var filteredItems = items.Where(x => x.InventoryId == rental.InventoryID).ToList();
            Inventory fItem = filteredItems[0];
            this.items.Add(fItem);
            dynamic info = new ExpandoObject();
            info.ModelName = fItem.ModelName;
            info.InventoryId = aReturn.InventoryID;
            info.QuantityReturned = aReturn.Quantity_Returned;
            info.RentalID = aReturn.RentalTransactionID;
            this.returnDisplay.Add(info);
            DataTable infos = this.ToDataTable(this.returnDisplay);
            this.dataGridView1.DataSource = infos;
            this.returnButton.Visible = false;
            this.returns.Add(aReturn);
            this.returnButton.Visible = true;
            this.returnButton.Enabled = true;
           
        }

        private void Return_Click(object sender, EventArgs e)
        {
            int index = 0;
            decimal payment = 0;
            foreach (Return aReturn in this.returns)
            {
                Transaction transaction = new Transaction();
                transaction.CustomerID = this.currCustomer.CustomerID;
                transaction.EmployeeID = this.currEmployee.EmployeeID;
                transaction.DateTime = DateTime.Now;
                transaction.IsReturn = true;
                transaction.TransactionID = aReturn.ReturnTransactionID + index;

                if (transaction.DateTime > this.rentals[index].DueDate)
                {
                    TimeSpan diff = transaction.DateTime - this.rentals[index].DueDate;
                    decimal sum = Convert.ToInt32(diff.Days) * this.items[index].FineRate;
                    transaction.Payment = sum;
                    payment += sum;
                }
                else
                {
                    transaction.Payment = 0;
                }
                List<Return> rets = new List<Return>();
                rets.Add(aReturn);
                this.tranCon.addReturn(transaction, rets);
                this.invenCon.IncrementInventory(this.items[index].InventoryId, aReturn.Quantity_Returned);
                index++;
            }
            this.Dispose();
            MessageBox.Show(payment.ToString());
        }

        private DataTable ToDataTable(IEnumerable<dynamic> items)
        {
            var data = items.ToArray();
            if (data.Count() == 0) return null;

            var dt = new DataTable();
            foreach (var key in ((IDictionary<string, object>)data[0]).Keys)
            {

                dt.Columns.Add(key);


            }
            foreach (var d in data)
            {

                dt.Rows.Add(((IDictionary<string, object>)d).Values.ToArray());

            }
            return dt;
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            int lastIndex = this.rentals.Count - 1;
            this.rentals.RemoveAt(lastIndex);
            this.items.RemoveAt(lastIndex);
            this.returns.RemoveAt(lastIndex);
            this.returnDisplay.RemoveAt(lastIndex);
            DataTable dt = this.ToDataTable(this.returnDisplay);
            this.dataGridView1.DataSource = dt;
        }
    }
}
