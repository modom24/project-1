﻿using OdomYangProject1.Controller;
using OdomYangProject1.DAL.Repository;
using OdomYangProject1.Model;
using OdomYangProject1.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OdomYangProject1
{
    public partial class LoginForm : Form
    {
        
        
        private MainForm mainForm;
        private EmployeeController employeeController;
        private AdminController adminController;
        public LoginForm(MainForm mainForm)
        {
            InitializeComponent();
            this.passwordTextbox.PasswordChar = '*';
            this.mainForm = mainForm;
            this.adminController = new AdminController();
            this.employeeController = new EmployeeController();
            this.mainForm.Hide();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            try
            {
                string username = usernameTextbox.Text;
                string password = passwordTextbox.Text;
                if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password))
                {

                    MessageBox.Show("Please enter a valid username or password", "Invalid Input");
                }
                else
                {
                    checkForValidEmployeeOrAdmin(username, password);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Login Error" + ex.Message);
            }
            
        }

        private void checkForValidEmployeeOrAdmin(string username,string password) {

            if (this.adminController.DoesAdminExist(username, password))
            {
                this.Hide();
                AdminForm adminForm = new AdminForm();
                adminForm.Show();
            }
            else if (this.employeeController.DoesEmployeeExist(username, password))
            {
                this.mainForm.CurrentEmployee = this.employeeController.GetLoggedInEmployee(username, password);
                this.mainForm.WindowState = FormWindowState.Normal;
                this.mainForm.Enabled = true;
                this.mainForm.Text = "Currently Logged In: " + this.mainForm.CurrentEmployee.Fname;
                this.passwordTextbox.Text = "";
                this.Hide();
                this.mainForm.Show();
            }
            else {
                MessageBox.Show("No matching Username or Password", "Cannot Find Employee or Administrator");
            }


        }

        private void LoginForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.mainForm.Close();
        }
    }
}
