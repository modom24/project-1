﻿namespace OdomYangProject1.View
{
    partial class RegistrationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.customerFnameTextbox = new System.Windows.Forms.TextBox();
            this.customerLnameTextbox = new System.Windows.Forms.TextBox();
            this.customerPhoneTextbox = new System.Windows.Forms.TextBox();
            this.customerAddressTwoTextbox = new System.Windows.Forms.TextBox();
            this.customerSSNTextbox = new System.Windows.Forms.TextBox();
            this.customerAddressTextbox = new System.Windows.Forms.TextBox();
            this.customerCityTextbox = new System.Windows.Forms.TextBox();
            this.customerZipTextbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.customerStateComboBox = new System.Windows.Forms.ComboBox();
            this.registerCustomerButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // customerFnameTextbox
            // 
            this.customerFnameTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerFnameTextbox.Location = new System.Drawing.Point(155, 70);
            this.customerFnameTextbox.Name = "customerFnameTextbox";
            this.customerFnameTextbox.Size = new System.Drawing.Size(135, 24);
            this.customerFnameTextbox.TabIndex = 0;
            // 
            // customerLnameTextbox
            // 
            this.customerLnameTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerLnameTextbox.Location = new System.Drawing.Point(155, 98);
            this.customerLnameTextbox.Name = "customerLnameTextbox";
            this.customerLnameTextbox.Size = new System.Drawing.Size(135, 24);
            this.customerLnameTextbox.TabIndex = 1;
            // 
            // customerPhoneTextbox
            // 
            this.customerPhoneTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerPhoneTextbox.Location = new System.Drawing.Point(155, 128);
            this.customerPhoneTextbox.Name = "customerPhoneTextbox";
            this.customerPhoneTextbox.Size = new System.Drawing.Size(135, 24);
            this.customerPhoneTextbox.TabIndex = 2;
            // 
            // customerAddressTwoTextbox
            // 
            this.customerAddressTwoTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerAddressTwoTextbox.Location = new System.Drawing.Point(155, 218);
            this.customerAddressTwoTextbox.Name = "customerAddressTwoTextbox";
            this.customerAddressTwoTextbox.Size = new System.Drawing.Size(135, 24);
            this.customerAddressTwoTextbox.TabIndex = 5;
            // 
            // customerSSNTextbox
            // 
            this.customerSSNTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerSSNTextbox.Location = new System.Drawing.Point(155, 158);
            this.customerSSNTextbox.Name = "customerSSNTextbox";
            this.customerSSNTextbox.Size = new System.Drawing.Size(135, 24);
            this.customerSSNTextbox.TabIndex = 3;
            // 
            // customerAddressTextbox
            // 
            this.customerAddressTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerAddressTextbox.Location = new System.Drawing.Point(155, 188);
            this.customerAddressTextbox.Name = "customerAddressTextbox";
            this.customerAddressTextbox.Size = new System.Drawing.Size(135, 24);
            this.customerAddressTextbox.TabIndex = 4;
            // 
            // customerCityTextbox
            // 
            this.customerCityTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerCityTextbox.Location = new System.Drawing.Point(155, 248);
            this.customerCityTextbox.Name = "customerCityTextbox";
            this.customerCityTextbox.Size = new System.Drawing.Size(135, 24);
            this.customerCityTextbox.TabIndex = 6;
            // 
            // customerZipTextbox
            // 
            this.customerZipTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerZipTextbox.Location = new System.Drawing.Point(155, 310);
            this.customerZipTextbox.Name = "customerZipTextbox";
            this.customerZipTextbox.Size = new System.Drawing.Size(135, 24);
            this.customerZipTextbox.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(27, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 20);
            this.label1.TabIndex = 11;
            this.label1.Text = "First Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(27, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 20);
            this.label2.TabIndex = 12;
            this.label2.Text = "Last Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(27, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 20);
            this.label3.TabIndex = 13;
            this.label3.Text = "Phone Number";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(27, 160);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 20);
            this.label4.TabIndex = 14;
            this.label4.Text = "SSN";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(27, 190);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 20);
            this.label6.TabIndex = 16;
            this.label6.Text = "Address";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(27, 220);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 20);
            this.label7.TabIndex = 17;
            this.label7.Text = "Address 2";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(27, 312);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 20);
            this.label8.TabIndex = 18;
            this.label8.Text = "Zip Code";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(27, 250);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 20);
            this.label9.TabIndex = 19;
            this.label9.Text = "City";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(27, 280);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 20);
            this.label10.TabIndex = 20;
            this.label10.Text = "State";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(49, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(229, 26);
            this.label5.TabIndex = 21;
            this.label5.Text = "Customer Registration";
            // 
            // customerStateComboBox
            // 
            this.customerStateComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.customerStateComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerStateComboBox.FormattingEnabled = true;
            this.customerStateComboBox.Items.AddRange(new object[] {
            "AL",
            "AK",
            "AZ",
            "AR",
            "CA",
            "CO",
            "CT",
            "DE",
            "FL",
            "GA",
            "HI",
            "ID",
            "IL",
            "IN",
            "IA",
            "KS",
            "KY",
            "LA",
            "ME",
            "MD",
            "MA",
            "MI",
            "MN",
            "MS",
            "MO",
            "MT",
            "NE",
            "NV",
            "NH",
            "NJ",
            "NM",
            "NY",
            "NC",
            "ND",
            "OH",
            "OK",
            "OR",
            "PA",
            "RI",
            "SC",
            "SD",
            "TN",
            "TX",
            "UT",
            "VT",
            "VA",
            "WA",
            "WV",
            "WI",
            "WY",
            "DC"});
            this.customerStateComboBox.Location = new System.Drawing.Point(155, 278);
            this.customerStateComboBox.Name = "customerStateComboBox";
            this.customerStateComboBox.Size = new System.Drawing.Size(135, 26);
            this.customerStateComboBox.TabIndex = 7;
            // 
            // registerCustomerButton
            // 
            this.registerCustomerButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.registerCustomerButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.registerCustomerButton.Location = new System.Drawing.Point(40, 358);
            this.registerCustomerButton.Name = "registerCustomerButton";
            this.registerCustomerButton.Size = new System.Drawing.Size(238, 42);
            this.registerCustomerButton.TabIndex = 23;
            this.registerCustomerButton.Text = "Register";
            this.registerCustomerButton.UseVisualStyleBackColor = true;
            this.registerCustomerButton.Click += new System.EventHandler(this.registerCustomerButton_Click);
            // 
            // RegistrationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(327, 412);
            this.Controls.Add(this.registerCustomerButton);
            this.Controls.Add(this.customerStateComboBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.customerZipTextbox);
            this.Controls.Add(this.customerCityTextbox);
            this.Controls.Add(this.customerAddressTextbox);
            this.Controls.Add(this.customerSSNTextbox);
            this.Controls.Add(this.customerAddressTwoTextbox);
            this.Controls.Add(this.customerPhoneTextbox);
            this.Controls.Add(this.customerLnameTextbox);
            this.Controls.Add(this.customerFnameTextbox);
            this.Name = "RegistrationForm";
            this.Text = "RegistrationForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox customerFnameTextbox;
        private System.Windows.Forms.TextBox customerLnameTextbox;
        private System.Windows.Forms.TextBox customerPhoneTextbox;
        private System.Windows.Forms.TextBox customerAddressTwoTextbox;
        private System.Windows.Forms.TextBox customerSSNTextbox;
        private System.Windows.Forms.TextBox customerAddressTextbox;
        private System.Windows.Forms.TextBox customerCityTextbox;
        private System.Windows.Forms.TextBox customerZipTextbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox customerStateComboBox;
        private System.Windows.Forms.Button registerCustomerButton;
    }
}