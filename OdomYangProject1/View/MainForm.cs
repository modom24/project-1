﻿using System;
using System.Windows.Forms;
using OdomYangProject1.View;
using OdomYangProject1.Controller;
using OdomYangProject1.Model;
using System.Collections.Generic;
using System.Drawing.Text;
using System.Linq;
using System.Runtime.InteropServices;

namespace OdomYangProject1
{
    public partial class MainForm : Form
    {
        public Employee CurrentEmployee;
        private Customer CurrentCustomer;
        private CustomerController customerController;
        private InventoryController inventoryController;
        private LoginForm loginForm;
        private RentalForm rentalForm;
        private TransactionController transactionController;
        private ReturnForm returnForm;
        private Boolean isRental;
        public MainForm()
        {
           this.customerController = new CustomerController();
           this.inventoryController = new InventoryController();
           this.transactionController = new TransactionController();
           InitializeComponent();
           this.WindowState = FormWindowState.Minimized;
           this.ShowInTaskbar = false;
           this.Enabled = false;
           this.loginForm = new LoginForm(this);
           this.loginForm.Show();
           this.rentButton.Enabled = false;
           this.customerSearchTextBox.Text = "";
           this.customerLnameSearchTextBox.Text = "";
           this.customerTransactionButton.Visible = false;
           this.dataGridView1.ReadOnly = true;
           
           this.rentalMenuItem.Enabled = false;
           this.returnMenuItem.Enabled = false;

        }
        

        private void searchTextBox_Click(object sender, EventArgs e)
        {
            this.searchTextBox.Clear();
            
        }
        private void searchTextBox_Left(object sender, EventArgs e)
        {
            if (this.searchTextBox.Text.Equals(""))
            {
                this.searchTextBox.Text = @"Search";
            }
        }

        private void registerCustomerButton_Click(object sender, EventArgs e)
        {
            RegistrationForm registrationForm = new RegistrationForm();
            registrationForm.Show();
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(this.searchTextBox.Text) || string.IsNullOrWhiteSpace(this.searchItemComboBox.Text))
                {
                    MessageBox.Show("Search criteria cannot be empty");
                }
                else if (this.searchItemComboBox.Text == "Inventory #" && !verifyNumericInput(this.searchTextBox.Text))
                {
                    MessageBox.Show("Inventory # must be a number");
                }
                else
                {
                    List<Inventory> sortedInventory = this.inventoryController.SortInventoryByCriteria(this.searchItemComboBox.Text, this.searchTextBox.Text);
                    if (sortedInventory.Count == 0)
                    {
                        MessageBox.Show("No items in inventory matching search criteria");
                    }
                    else
                    {
                        this.dataGridView1.DataSource = sortedInventory;
                        this.rentalMenuItem.Enabled = true;
                    }
                }
            }
            catch (Exception)
            {

                MessageBox.Show("Error searching database");
            }
        }

        private Boolean verifyNumericInput(string userInput)
        {
            if (userInput.All(char.IsDigit))
            {
                return true;
            }

            return false;
        }

        private void rentalMenuItem_Click(object sender, EventArgs e)
        {
            List<Inventory> selectedInventory = new List<Inventory>();
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                Inventory inventoryItem = row.DataBoundItem as Inventory;
                selectedInventory.Add(inventoryItem);
            }
            this.rentButton.Enabled = true;
            this.rentalForm = new RentalForm(selectedInventory,this.CurrentEmployee);
            rentalForm.Show();
            this.isRental = true;
            this.rentButton.Text = "Rent";

        }

        private void newCustomerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            registerCustomerButton_Click(sender, e);
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            CurrentEmployee = null;
            this.loginForm.Show();
            this.Hide();
            this.Enabled = false;
        }

        private void aboutThisProgramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This program allows for the registering of customers, validation of employees, and searching of items by certain criteria");
        }

        private void rentButton_Click(object sender, EventArgs e)
        {
            if(this.isRental == true)
            {
                this.rentals();
            }
            else
            {
                this.returns();
            }
        }

        private void returnMenuItem_Click(object sender, EventArgs e)
        {
            this.returnForm = new ReturnForm(this.CurrentCustomer,this.CurrentEmployee);
            this.returnForm.Show();
            this.isRental = false;
        }

        private void searchCustomerButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(this.searchCustomerDropDown.Text))
            {
                MessageBox.Show("please choose a criteria");
            }
            else if (this.searchCustomerDropDown.Text == "Name")
            {
                string fname = this.customerSearchTextBox.Text;
                string lname = this.customerLnameSearchTextBox.Text;
                List<Customer> customers = this.customerController.GetCustomerByName(lname, fname);
                this.dataGridView1.DataSource = customers;
                this.checkCustomerListSize(customers);
                this.customerTransactionButton.Visible = true;
                this.customerTransactionButton.Enabled = true;
            }
            else if (this.searchCustomerDropDown.Text == "Phone#" && verifyNumericInput(this.customerSearchTextBox.Text) && this.customerSearchTextBox.Text.Length == 10)
            {
                long phoneNumber = (long)Convert.ToInt64(this.customerSearchTextBox.Text);
                List<Customer> customers = this.customerController.GetCustomerByPhone(phoneNumber);
                this.dataGridView1.DataSource = customers;
                this.checkCustomerListSize(customers);
                this.customerTransactionButton.Visible = true;
                this.customerTransactionButton.Enabled = true;
            }
            else if (this.searchCustomerDropDown.Text == "Id" && verifyNumericInput(this.customerSearchTextBox.Text))
            {
                int id = Convert.ToInt32(this.customerSearchTextBox.Text);
                Customer customer = this.customerController.GetCustomerByCustomerID(id);
                List<Customer> customers = new List<Customer>();
                customers.Add(customer);
                this.dataGridView1.DataSource = customers;
                this.checkCustomerListSize(customers);
                this.customerTransactionButton.Visible = true;
                this.customerTransactionButton.Enabled = true;
            }
            else
            {
                MessageBox.Show("please input valid search inputs");
            }

        }

        private void searchCustomerDropDown_DropDownClosed(object sender, EventArgs e)
        {
            if (this.searchCustomerDropDown.Text == "Name")
            {
                this.customerLnameSearchTextBox.Enabled = true;
                
            }
            else
            {
                this.customerLnameSearchTextBox.Text = "";
                this.customerLnameSearchTextBox.Enabled = false;
                
            }
        }

        private void customerTransactionButton_Click(object sender, EventArgs e)
        {
            List<Transaction> transactions  = new List<Transaction>();
            List<Rental> activeRentals = new List<Rental>();
            Customer selectedCustomer = new Customer();
            int sum = 0;
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                selectedCustomer = row.DataBoundItem as Customer;
                this.CurrentCustomer = selectedCustomer;
                transactions = this.transactionController.GetTransactionsByCustomer(selectedCustomer.CustomerID);
            }
            
            var rentalTransactions = from t in transactions
                          where t.IsReturn == false
                          select t;
             var returnTransactions = from t in transactions
                          where t.IsReturn == true
                          select t;

            List<Rental> allRentals = this.transactionController.GetAllRentals();
            List<Return> allReturns = this.transactionController.GetAllReturns();
            List<Rental> rentals = allRentals.Where(x => rentalTransactions.Any(y => y.TransactionID == x.RentalTransactionID)).ToList();
            List<Return> returns = allReturns.Where(x => returnTransactions.Any(y => y.TransactionID == x.ReturnTransactionID)).ToList();
            if (returns.Count == 0)
            {
                this.dataGridView1.DataSource = rentals;
                this.customerTransactionButton.Visible = false;
            }

            List<Rental> activeRents = new List<Rental>();
            
              foreach(Rental rent in rentals)
            {
                sum = 0;
                List<Return> rets = this.transactionController.GetReturnsByRentalID(rent.RentalTransactionID, rent.InventoryID);
                foreach(Return aRental in rets) {
                    sum += aRental.Quantity_Returned;
                }

                if(sum < rent.Qty)
                {
                    activeRents.Add(rent);
                }
            }

          

         

            if (activeRents.Count > 0)
            {
                this.dataGridView1.DataSource = activeRents;
                this.rentButton.Enabled = true;
                this.customerTransactionButton.Enabled = false;
                this.returnMenuItem.Enabled = true;
                this.rentButton.Text = "Return";

            }
           

        }

        private void checkCustomerListSize(List<Customer> customers)
        {
            if (customers.Count > 0)
            {
                this.customerTransactionButton.Visible = true;
            }
        }

        private void returns()
        {
            Rental ret = new Rental();
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                ret = row.DataBoundItem as Rental;

            }


            if (this.returnForm != null && verifyNumericInput(this.quantitySelector.Text))
            {
                int qty = Convert.ToInt32(this.quantitySelector.Text);
                if (qty <= ret.Qty)
                {
                    this.returnForm.AddReturns(ret, qty);
                }
                else
                {
                    MessageBox.Show("Invalid quantity. Returns cannot be greater Rental");
                }
            }
            else
            {
                MessageBox.Show("Please input valid quantity");
            }
        }


        private void rentals()
        {
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                Inventory inventoryItem = row.DataBoundItem as Inventory;
                Rental rental = new Rental();
                if (inventoryItem != null)
                {
                    rental.InventoryID = inventoryItem.InventoryId;
                    rental.Qty = Convert.ToInt32(this.quantitySelector.Value);
                    rental.DueDate = DateTime.Now;
                    this.rentalForm.addRental(inventoryItem, rental);
                }
                else
                {
                    MessageBox.Show("No items selected");
                }

            }
        }

       
    
        
    }
}
